package com.example.jiaji.chauffeurprive.ui.presenters;

import android.content.Context;
import com.example.jiaji.chauffeurprive.ui.fragments.MainFragment;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(MockitoJUnitRunner.class)
public class MainFragmentPresenterUnitTest {

    @Mock
    Context context;
    @Mock
    MainFragment mainFragment;

    MainFragmentPresenter presenter;

    @Before
    public void init() throws Exception {
        presenter = new MainFragmentPresenter(mainFragment, context);
    }

    @Test
    public void testGetAutoCompletion() {
        presenter.getAutoCompletion("chatelet", 48.22, 2.23);
        assertFalse(presenter.getSearchDisposable().isDisposed());
        presenter.resetSearch();
        assertTrue(presenter.getSearchDisposable().isDisposed());
    }
}