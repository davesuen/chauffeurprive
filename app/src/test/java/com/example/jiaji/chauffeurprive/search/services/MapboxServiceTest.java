package com.example.jiaji.chauffeurprive.search.services;

import com.example.jiaji.chauffeurprive.search.models.Feature;
import com.example.jiaji.chauffeurprive.search.models.Properties;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import java.util.ArrayList;
import java.util.List;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(MockitoJUnitRunner.class)
public class MapboxServiceTest {

    @Mock
    MapboxService service;

    @Test
    public void testGetAutocompletion() {

        List<Feature> features = new ArrayList<>();
        double[] coordinates = {43.1233, 2.1933};
        features.add(new Feature("id1", "text1","placeName1", coordinates, new Properties("address", "category")));
        features.add(new Feature("id2", "text2","placeName2", coordinates, new Properties("address", "category")));
        features.add(new Feature("id3", "text3","placeName3", coordinates, new Properties("address", "category")));
        Mockito.doReturn(Observable.just(features)).when(service).getAutocompletion("chatelet", "2.34,48.92");

        TestObserver<List<Feature>> testObserver = new TestObserver();
        service.getAutocompletion("chatelet", "2.34,48.92").flatMap(Observable::fromIterable)
                .toList().subscribe(testObserver);
        testObserver.assertComplete();
        testObserver.assertNoErrors();
        testObserver.assertValueCount(1);
        assertEquals(testObserver.values().get(0).size(), 3);
        assertEquals(testObserver.values().get(0).get(2).id, "id3");
    }
}