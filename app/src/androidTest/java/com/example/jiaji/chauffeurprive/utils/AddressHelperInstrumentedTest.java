package com.example.jiaji.chauffeurprive.utils;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.example.jiaji.chauffeurprive.address.models.AddressItem;
import com.orhanobut.hawk.Hawk;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class AddressHelperInstrumentedTest {

    private static final double DELTA = 1e-15;

    @Before
    public void setUp() {
        Context context = InstrumentationRegistry.getTargetContext();
        AddressHelper.init(context);
    }

    @Test
    public void testsSaveAddressList() {
        AddressHelper.addAddress(new AddressItem(43.2222, 2.2456, "43 rue Courtois, 75004 Paris"));
        ArrayList<AddressItem> addressItems = AddressHelper.getAddressList();
        assertEquals(addressItems.get(addressItems.size() - 1).getLatitude(), 43.2222, DELTA);
        assertEquals(addressItems.get(addressItems.size() - 1).getLongitude(), 2.2456, DELTA);
        assertEquals(addressItems.get(addressItems.size() - 1).getTitle(), "43 rue Courtois, 75004 Paris");
    }
}
