package com.example.jiaji.chauffeurprive.search.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Jiaji on 22/12/2017.
 */

public class Properties {
    public final String category;
    public final String address;

    @JsonCreator
    public Properties(@JsonProperty("address") String address, @JsonProperty("category") String category) {
        this.address = address;
        this.category = category;
    }
}
