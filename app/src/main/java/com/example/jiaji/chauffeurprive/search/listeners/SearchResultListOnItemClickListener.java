package com.example.jiaji.chauffeurprive.search.listeners;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

public class SearchResultListOnItemClickListener extends RecyclerView.SimpleOnItemTouchListener {

    private OnItemClickListener listener;
    private GestureDetector gestureDetector;

    public SearchResultListOnItemClickListener(Context context, OnItemClickListener listener) {
        this.listener = listener;
        gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        View child = rv.findChildViewUnder(e.getX(), e.getY());
        int position = rv.getChildAdapterPosition(child);
        if (position != RecyclerView.NO_POSITION && gestureDetector.onTouchEvent(e)) {
            listener.onItemClick(position);
        }
        return false;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }
}
