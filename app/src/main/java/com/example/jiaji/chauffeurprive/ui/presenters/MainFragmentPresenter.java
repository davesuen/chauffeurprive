package com.example.jiaji.chauffeurprive.ui.presenters;

import android.content.Context;

import com.example.jiaji.chauffeurprive.search.services.MapboxService;
import com.example.jiaji.chauffeurprive.ui.contracts.MainFragmentContract;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

import static java.util.Collections.emptyList;

/**
 * Created by Jiaji on 20/01/2018.
 */

public class MainFragmentPresenter implements MainFragmentContract.UserActionListener {

    private static final String TAG = "MainFragmentPresenter";

    private static final int AUTOCOMPLETE_DELAY = 1000;

    private MainFragmentContract.View view;

    private Disposable searchDisposable;
    private MapboxService mapboxService;

    public MainFragmentPresenter(MainFragmentContract.View view, Context context) {
        this.view = view;
        mapboxService = new MapboxService(context);
    }

    @Override
    public void getAutoCompletion(String queryString, double latitude, double longitude) {
        searchDisposable = mapboxService.getAutocompletion(queryString, longitude + "," + latitude)
                .delay(AUTOCOMPLETE_DELAY, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                .flatMap(Observable::fromIterable)
                .toList()
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(it -> view.setAutocompletion(it),
                        e -> {
                            view.setAutocompletion(emptyList());
                            Timber.e("Cannot get predictions: %s", e.getMessage());
                        });
    }

    @Override
    public void resetSearch() {
        if (searchDisposable != null) searchDisposable.dispose();
    }

    public Disposable getSearchDisposable() {
        return searchDisposable;
    }
}
