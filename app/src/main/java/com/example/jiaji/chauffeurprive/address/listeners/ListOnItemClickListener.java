package com.example.jiaji.chauffeurprive.address.listeners;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by jiaji on 25/08/2016.
 */

public class ListOnItemClickListener extends RecyclerView.SimpleOnItemTouchListener {
    private GestureDetector gestureDetector;
    private OnItemClickListener listener;

    public ListOnItemClickListener(Context context, OnItemClickListener listener) {
        this.listener = listener;
        gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        View child = rv.findChildViewUnder(e.getX(), e.getY());
        int position = rv.getChildAdapterPosition(child);
        if (position != RecyclerView.NO_POSITION && gestureDetector.onTouchEvent(e)) {
            listener.onItemClick(position);
        }
        return false;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }
}