package com.example.jiaji.chauffeurprive.utils;

import android.content.Context;

import com.example.jiaji.chauffeurprive.address.models.AddressItem;
import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;

/**
 * Created by Jiaji on 16/01/2018.
 */

public class AddressHelper {
    private static final String ADDRESS_CACHE_KEY = "address";
    public static final int MAX_SIZE = 15;

    public static void init(Context context) {
        Hawk.init(context).build();
    }

    public static void saveAddressList(ArrayList<AddressItem> addressItems) {
        if (addressItems != null) {
            int size = addressItems.size();
            if (size > 15) {
                Hawk.put(ADDRESS_CACHE_KEY, addressItems.subList(0, MAX_SIZE));
            } else {
                Hawk.put(ADDRESS_CACHE_KEY, addressItems);
            }
        }
    }

    public static ArrayList<AddressItem> getAddressList() {
        Object o =  Hawk.get(ADDRESS_CACHE_KEY, null);
        if (o != null && o instanceof ArrayList) {
            return (ArrayList) o;
        } else {
            return new ArrayList<>();
        }
    }

    public static void addAddress(AddressItem addressItem) {
        ArrayList<AddressItem> addressItems = getAddressList();
        if (addressItems == null) {
            addressItems = new ArrayList<>();
        }
        int size = addressItems.size();
        if (size >= MAX_SIZE) {
            addressItems.remove(0);
        }
        addressItems.add(addressItem);
        saveAddressList(addressItems);
    }
}
