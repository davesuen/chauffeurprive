package com.example.jiaji.chauffeurprive.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jiaji.chauffeurprive.address.adapters.AddressAdapter;
import com.example.jiaji.chauffeurprive.address.listeners.AddressOnClickListener;
import com.example.jiaji.chauffeurprive.address.listeners.ListOnItemClickListener;
import com.example.jiaji.chauffeurprive.ui.fragments.listeners.FragmentOnResumeListener;
import com.example.jiaji.chauffeurprive.R;
import com.example.jiaji.chauffeurprive.address.models.AddressItem;
import com.example.jiaji.chauffeurprive.utils.AddressHelper;

/**
 * Created by Jiaji on 15/01/2018.
 */

public class AddressFragment extends Fragment implements ListOnItemClickListener.OnItemClickListener {

    public static final String TAG = "AddressFragment";
    private FragmentOnResumeListener fragmentOnResumeListener;
    private AddressOnClickListener addressOnClickListener;
    private AddressAdapter addressAdapter;

    public static AddressFragment newInstance() {
        Log.i(TAG, "newInstance");
        AddressFragment fragment = new AddressFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG,"onCreateView");
        View view = inflater.inflate(R.layout.address_fragment, container, false);
        if (fragmentOnResumeListener == null && getActivity() instanceof FragmentOnResumeListener) {
            fragmentOnResumeListener = (FragmentOnResumeListener) getActivity();
        }
        if (addressOnClickListener == null && getActivity() instanceof AddressOnClickListener) {
            addressOnClickListener = (AddressOnClickListener) getActivity();
        }
        RecyclerView addressListRecyclerView = view.findViewById(R.id.addressList);
        addressAdapter = new AddressAdapter(AddressHelper.getAddressList());
        addressListRecyclerView.setAdapter(addressAdapter);
        addressListRecyclerView.addOnItemTouchListener(new ListOnItemClickListener(getContext(), this));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(addressListRecyclerView.getContext(),
                LinearLayoutManager.VERTICAL);
        addressListRecyclerView.addItemDecoration(dividerItemDecoration);
        return view;
    }

    @Override
    public void onResume() {
        Log.i(TAG, "onResume");
        super.onResume();
        if (fragmentOnResumeListener != null) {
            fragmentOnResumeListener.onResumeFragment(this);
        }

        addressAdapter.setAddressItems(AddressHelper.getAddressList());
    }

    public void addAddressItem(AddressItem addressItem) {
        addressAdapter.addAddressItem(addressItem);
    }

    public FragmentOnResumeListener getFragmentOnResumeListener() {
        return fragmentOnResumeListener;
    }

    public void setFragmentOnResumeListener(FragmentOnResumeListener fragmentOnResumeListener) {
        this.fragmentOnResumeListener = fragmentOnResumeListener;
    }

    @Override
    public void onItemClick(int position) {
        if (addressOnClickListener != null) {
            addressOnClickListener.onAddressClicked(AddressHelper.getAddressList().get(position));
        }
    }
}