package com.example.jiaji.chauffeurprive.map.managers;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import com.example.jiaji.chauffeurprive.R;
import com.example.jiaji.chauffeurprive.map.Listeners.LocationListener;
import com.example.jiaji.chauffeurprive.map.providers.MapProvider;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerViewOptions;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.plugins.locationlayer.LocationLayerMode;
import com.mapbox.mapboxsdk.plugins.locationlayer.LocationLayerPlugin;
import com.mapbox.services.android.telemetry.permissions.PermissionsManager;

/**
 * Created by Jiaji on 21/01/2018.
 */

public class MapboxManager extends MapProvider implements MapboxMap.OnCameraMoveListener, LocationListener {

    private MapView mapView;
    private Context context;
    private MapboxMap mapboxMap;
    private MapboxLocationManager mapboxLocationManager;
    private LocationLayerPlugin locationPlugin;
    private Marker marker;

    public MapboxManager(MapView mapView, Context context) {
        this.mapView = mapView;
        this.context = context;
    }

    @Override
    public void initMap(Bundle savedInstanceState) {
        mapboxLocationManager = new MapboxLocationManager(this, context);
        Mapbox.getInstance(context, context.getString(R.string.access_token));
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(mapboxMap -> {
            this.mapboxMap = mapboxMap;
            activeLocation();
            this.mapboxMap.addOnCameraMoveListener(this);
        });
    }

    @SuppressWarnings( {"MissingPermission"})
    @Override
    public void activeLocation() {
        // Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(context)) {
            // Create an instance of LOST location engine
            if (locationPlugin == null) {
                mapboxLocationManager.initLocation();
                locationPlugin = new LocationLayerPlugin(mapView, mapboxMap, mapboxLocationManager.getLocationEngine());
                locationPlugin.setLocationLayerEnabled(LocationLayerMode.TRACKING);
            }
        }
    }

    @Override
    public void navigateCameraToPosition(Location location) {
        mapboxMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(location.getLatitude(), location.getLongitude()), 13));
    }

    @Override
    public void navigateCameraToPosition(com.example.jiaji.chauffeurprive.map.models.LatLng latLng) {
        mapboxMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(latLng.getLatitude(), latLng.getLongitude()), 13));
    }

    @Override
    public void addMarker(double lat, double lng, String title, int markerDrawableId) {
        LatLng latLng = new LatLng(lat, lng);
        if (marker == null) {
            MarkerViewOptions markerOptions = new MarkerViewOptions();
            mapboxMap.addMarker(markerOptions.position(latLng));
            marker = markerOptions.getMarker();
        } else {
            marker.setPosition(latLng);
            marker.setTitle(title);
        }
    }

    @Override
    public void onCameraMove() {
        marker.setPosition(mapboxMap.getCameraPosition().target);
    }

    @Override
    public void onLocationChanged(Location location) {
        navigateCameraToPosition(location);
        addMarker(location.getLatitude(), location.getLongitude(), null, 0);
    }

    @Override
    public com.example.jiaji.chauffeurprive.map.models.LatLng getCurrentMarkerLocation() {
        return new com.example.jiaji.chauffeurprive.map.models.LatLng(marker.getPosition().getLatitude(), marker.getPosition().getLongitude());
    }

    @Override
    public String getCurrentMarkerAddress() {
        return marker.getTitle();
    }

    public MapboxLocationManager getMapboxLocationManager() {
        return mapboxLocationManager;
    }

    @SuppressWarnings( {"MissingPermission"})
    @Override
    public void registerLocationChange() {
        mapboxLocationManager.registerLocationChange();
    }

    @Override
    public void unregisterLocationChange() {
        mapboxLocationManager.unregisterLocationChange();
    }

    @Override
    public boolean isUserLocationAccessible() {
        return mapboxLocationManager.isUserLocationAccessible();
    }

    @Override
    public void onStart() {
        mapView.onStart();
    }

    @Override
    public void onStop() {
        mapView.onStop();
    }

    @Override
    public void onResume() {
        mapView.onResume();
    }

    @Override
    public void onPause() {
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        mapView.onDestroy();
        mapboxLocationManager.release();
    }

    @Override
    public void onLowMemory() {
        mapView.onLowMemory();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        mapView.onSaveInstanceState(outState);
    }
}
