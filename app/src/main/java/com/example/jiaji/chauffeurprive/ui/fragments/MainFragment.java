package com.example.jiaji.chauffeurprive.ui.fragments;

import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SearchView;
import com.example.jiaji.chauffeurprive.address.listeners.AddAddressListener;
import com.example.jiaji.chauffeurprive.address.models.AddressItem;
import com.example.jiaji.chauffeurprive.map.managers.MapboxManager;
import com.example.jiaji.chauffeurprive.map.models.LatLng;
import com.example.jiaji.chauffeurprive.map.providers.MapProvider;
import com.example.jiaji.chauffeurprive.search.models.Feature;
import com.example.jiaji.chauffeurprive.ui.contracts.MainFragmentContract;
import com.example.jiaji.chauffeurprive.ui.fragments.listeners.FragmentOnResumeListener;
import com.example.jiaji.chauffeurprive.R;
import com.example.jiaji.chauffeurprive.search.adapters.AutocompleteListAdapter;
import com.example.jiaji.chauffeurprive.search.listeners.SearchResultListOnItemClickListener;
import com.example.jiaji.chauffeurprive.ui.presenters.MainFragmentPresenter;
import com.example.jiaji.chauffeurprive.utils.AddressHelper;
import com.mapbox.mapboxsdk.maps.MapView;
import java.util.List;
import static java.util.Collections.emptyList;

/**
 * Created by Jiaji on 15/01/2018.
 */

public class MainFragment extends Fragment implements MainFragmentContract.View, SearchView.OnQueryTextListener, SearchResultListOnItemClickListener.OnItemClickListener {

    public static final String TAG = "MainFragment";
    private static final int MIN_SEARCH_TEXT_LENGTH = 5;

    private SearchView searchView;
    private RecyclerView searchResultRecyclerView;
    private AutocompleteListAdapter listAdapter;
    private FragmentOnResumeListener fragmentOnResumeListener;
    private AddAddressListener addAddressListener;
    private MainFragmentContract.UserActionListener presenter;
    private MapProvider mapProvider;

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new MainFragmentPresenter(this, getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_fragment, container, false);

        if (fragmentOnResumeListener == null && getActivity() instanceof FragmentOnResumeListener) {
            fragmentOnResumeListener = (FragmentOnResumeListener) getActivity();
        }
        if (addAddressListener == null && getActivity() instanceof AddAddressListener) {
            addAddressListener = (AddAddressListener) getActivity();
        }
        mapProvider = getMapProvider(view);
        mapProvider.initMap(savedInstanceState);

        initSearchView(view);
        initConfirmAddressButton(view);

        if (fragmentOnResumeListener == null && getActivity() instanceof FragmentOnResumeListener) {
            fragmentOnResumeListener = (FragmentOnResumeListener) getActivity();
        }
        return view;
    }

    private void initSearchView(View view) {
        searchView = view.findViewById(R.id.searchView);
        searchView.setOnClickListener(v -> searchView.onActionViewExpanded());
        searchView.setOnQueryTextListener(this);
        searchResultRecyclerView = view.findViewById(R.id.autoCompleteRecycleView);
        searchResultRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        searchResultRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        searchResultRecyclerView.addOnItemTouchListener(new SearchResultListOnItemClickListener(getContext(), this));
        listAdapter = new AutocompleteListAdapter(getContext());
        searchResultRecyclerView.setAdapter(listAdapter);
    }

    @SuppressWarnings( {"MissingPermission"})
    private void initConfirmAddressButton(View view) {
        Button confirmAddressButton = view.findViewById(R.id.confirmAddressButton);
        confirmAddressButton.setOnClickListener(button -> {
            LatLng latLng = mapProvider.getCurrentMarkerLocation();
            saveAddress(latLng.getLatitude(), latLng.getLongitude(), mapProvider.getCurrentMarkerAddress());
        });
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        if (listAdapter.getPredictionItemCount() == 0 && !query.isEmpty()) {
            onQueryTextChange(query);
        }
        return false;
    }

    @SuppressWarnings( {"MissingPermission"})
    @Override
    public boolean onQueryTextChange(String newText) {
        presenter.resetSearch();

        if (mapProvider.isUserLocationAccessible()) {
            if (!newText.isEmpty()) {
                if (newText.length() >= MIN_SEARCH_TEXT_LENGTH) {
                    LatLng latLng = mapProvider.getCurrentMarkerLocation();
                    presenter.getAutoCompletion(newText, latLng.getLatitude(), latLng.getLongitude());
                }
            } else {
                // Clear predictions
                setAutocompletion(emptyList());
            }
        }
        return false;
    }

    public void activeLocation() {
        mapProvider.activeLocation();
    }

    @Override
    public void setAutocompletion(List<Feature> data) {
        listAdapter.setPredictionList(data);
    }

    @Override
    public void onItemClick(int position) {
        if (position >= 0) {
            Location location = new Location("");
            double lat = listAdapter.getItem(position).center[1];
            double lon = listAdapter.getItem(position).center[0];
            location.setLongitude(lon);
            location.setLatitude(lat);

            mapProvider.addMarker(lat, lon, listAdapter.getItem(position).placeName, 0);

            searchView.clearFocus();
            listAdapter.clearPredictionList();
            mapProvider.navigateCameraToPosition(location);
        }
    }

    private void saveAddress(double lat, double lng, String addressTitle) {
        if (addressTitle == null) {
            addressTitle = lat + ", " + lng;
        }
        AddressItem addressItem = new AddressItem(lat, lng, addressTitle);
        AddressHelper.addAddress(addressItem);
        if (addAddressListener != null) {
            addAddressListener.onAddressAdded(addressItem);
        }
    }

    public void navigateToAddress(LatLng latLng) {
        mapProvider.navigateCameraToPosition(latLng);
    }

    @Override
    @SuppressWarnings( {"MissingPermission"})
    public void onStart() {
        super.onStart();
        mapProvider.registerLocationChange();
        mapProvider.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapProvider.unregisterLocationChange();
        mapProvider.onStop();
        presenter.resetSearch();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mapProvider.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapProvider.onLowMemory();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapProvider.onResume();
        if (fragmentOnResumeListener != null) {
            fragmentOnResumeListener.onResumeFragment(this);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mapProvider.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapProvider.onSaveInstanceState(outState);
    }

    public FragmentOnResumeListener getFragmentOnResumeListener() {
        return fragmentOnResumeListener;
    }

    public void setFragmentOnResumeListener(FragmentOnResumeListener fragmentOnResumeListener) {
        this.fragmentOnResumeListener = fragmentOnResumeListener;
    }

    public MapProvider getMapProvider(View view) {
        MapView mapView = view.findViewById(R.id.mapView);
        return new MapboxManager(mapView, getContext());
    }
}
