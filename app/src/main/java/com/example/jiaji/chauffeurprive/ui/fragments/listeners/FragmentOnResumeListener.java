package com.example.jiaji.chauffeurprive.ui.fragments.listeners;

import android.support.v4.app.Fragment;

/**
 * Created by Jiaji on 15/01/2018.
 */

public interface FragmentOnResumeListener {
    void onResumeFragment(Fragment fragment);
}
