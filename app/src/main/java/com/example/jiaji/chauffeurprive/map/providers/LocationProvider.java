package com.example.jiaji.chauffeurprive.map.providers;

import android.content.Context;
import com.example.jiaji.chauffeurprive.map.Listeners.LocationListener;

/**
 * Created by Jiaji on 21/01/2018.
 */

public abstract class LocationProvider {

    protected LocationListener locationListener;
    protected Context context;

    public LocationProvider(LocationListener locationListener, Context context) {
        this.locationListener = locationListener;
        this.context = context;
    }

    public abstract void initLocation();
    public abstract void registerLocationChange();
    public abstract void unregisterLocationChange();
    public abstract boolean isUserLocationAccessible();
    public abstract void release();
}