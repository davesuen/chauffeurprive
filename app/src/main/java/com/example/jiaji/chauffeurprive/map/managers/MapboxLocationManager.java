package com.example.jiaji.chauffeurprive.map.managers;

import android.content.Context;
import android.location.Location;

import com.example.jiaji.chauffeurprive.map.Listeners.LocationListener;
import com.example.jiaji.chauffeurprive.map.providers.LocationProvider;
import com.mapbox.services.android.telemetry.location.LocationEngine;
import com.mapbox.services.android.telemetry.location.LocationEngineListener;
import com.mapbox.services.android.telemetry.location.LocationEnginePriority;
import com.mapbox.services.android.telemetry.location.LostLocationEngine;

/**
 * Created by Jiaji on 21/01/2018.
 */

public class MapboxLocationManager extends LocationProvider implements LocationEngineListener {

    private LocationEngine locationEngine;

    public MapboxLocationManager(LocationListener locationListener, Context context) {
        super(locationListener, context);
    }

    @Override
    @SuppressWarnings( {"MissingPermission"})
    public void initLocation() {
        locationEngine = new LostLocationEngine(context);
        locationEngine.setPriority(LocationEnginePriority.HIGH_ACCURACY);
        locationEngine.activate();
        Location lastLocation = locationEngine.getLastLocation();
        if (lastLocation != null) {
            locationListener.onLocationChanged(lastLocation);
        } else {
            locationEngine.addLocationEngineListener(this);
        }
    }

    @Override
    @SuppressWarnings( {"MissingPermission"})
    public void onConnected() {
        if (locationEngine != null) {
            locationEngine.requestLocationUpdates();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        locationListener.onLocationChanged(location);
    }

    @SuppressWarnings( {"MissingPermission"})
    @Override
    public void registerLocationChange() {
        if (locationEngine != null) {
            locationEngine.requestLocationUpdates();
        }
    }

    @Override
    public void unregisterLocationChange() {
        if (locationEngine != null) {
            locationEngine.removeLocationUpdates();
        }
    }

    @SuppressWarnings( {"MissingPermission"})
    @Override
    public boolean isUserLocationAccessible() {
        return locationEngine != null && locationEngine.getLastLocation() != null;
    }

    @Override
    public void release() {
        if (locationEngine != null) {
            locationEngine.deactivate();
        }
    }

    public LocationEngine getLocationEngine() {
        return locationEngine;
    }

}
