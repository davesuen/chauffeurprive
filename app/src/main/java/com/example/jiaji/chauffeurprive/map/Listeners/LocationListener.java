package com.example.jiaji.chauffeurprive.map.Listeners;

import android.location.Location;

/**
 * Created by Jiaji on 21/01/2018.
 */

public interface LocationListener {
    void onLocationChanged(Location location);
}