package com.example.jiaji.chauffeurprive.search.services;

import com.example.jiaji.chauffeurprive.search.models.PlaceAutocomplete;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MapboxApi {

    @GET("{mode}/{query}.json")
    Observable<PlaceAutocomplete> getAutocompletion(
            @Path("query") String query,
            @Path("mode") String mode,
            @Query("access_token") String accessToken,
            @Query("proximity") String proximity,
            @Query("autocomplete") Boolean autocomplete,
            @Query("language") String language,
            @Query("limit") int limit);
}
