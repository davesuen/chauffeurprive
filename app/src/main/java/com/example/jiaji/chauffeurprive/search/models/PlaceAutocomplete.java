package com.example.jiaji.chauffeurprive.search.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collections;
import java.util.List;

public class PlaceAutocomplete {
    public final List<Feature> features;

    @JsonCreator
    public PlaceAutocomplete(@JsonProperty("features") List<Feature> features) {
        this.features = features == null ? null : Collections.unmodifiableList(features);
    }
}
