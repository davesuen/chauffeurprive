package com.example.jiaji.chauffeurprive.address.listeners;

import com.example.jiaji.chauffeurprive.address.models.AddressItem;

/**
 * Created by Jiaji on 16/01/2018.
 */

public interface AddAddressListener {
    void onAddressAdded(AddressItem addressItem);
}
