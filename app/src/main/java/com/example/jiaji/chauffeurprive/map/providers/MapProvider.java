package com.example.jiaji.chauffeurprive.map.providers;

import android.location.Location;
import android.os.Bundle;
import com.example.jiaji.chauffeurprive.map.models.LatLng;

/**
 * Created by Jiaji on 21/01/2018.
 */

public abstract class MapProvider {

    public abstract void initMap(Bundle savedInstanceState);
    public abstract void addMarker(double lat, double lng, String title, int markerDrawableId);
    public abstract void navigateCameraToPosition(Location location);
    public abstract void navigateCameraToPosition(LatLng latLng);
    public abstract LatLng getCurrentMarkerLocation();
    public abstract String getCurrentMarkerAddress();
    public abstract void registerLocationChange();
    public abstract void unregisterLocationChange();
    public abstract boolean isUserLocationAccessible();
    public abstract void activeLocation();
    public abstract void onStart();
    public abstract void onStop();
    public abstract void onResume();
    public abstract void onPause();
    public abstract void onDestroy();
    public abstract void onLowMemory();
    public abstract void onSaveInstanceState(Bundle outState);
}
