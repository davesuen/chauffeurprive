package com.example.jiaji.chauffeurprive.address.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.jiaji.chauffeurprive.R;
import com.example.jiaji.chauffeurprive.address.models.AddressItem;

import java.util.ArrayList;

import static com.example.jiaji.chauffeurprive.utils.AddressHelper.MAX_SIZE;

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolder> {
    private ArrayList<AddressItem> addressItems;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mTextView;
        public ViewHolder(TextView v) {
            super(v);
            mTextView = v;
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AddressAdapter(ArrayList<AddressItem> addressItems) {
        this.addressItems = addressItems;
    }

    public void addAddressItem(AddressItem addressItem) {
        if (addressItems.size() >= MAX_SIZE) {
            addressItems.remove(0);
        }
        addressItems.add(addressItem);
        notifyDataSetChanged();
    }

    public void setAddressItems(ArrayList<AddressItem> addressItems) {
        this.addressItems.clear();
        this.addressItems = addressItems;
        notifyDataSetChanged();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public AddressAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        TextView v = (TextView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.address_list_content, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.mTextView.setText(addressItems.get(position).getTitle());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return addressItems.size();
    }
}