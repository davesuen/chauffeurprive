package com.example.jiaji.chauffeurprive.search.services;

import android.content.Context;

import com.example.jiaji.chauffeurprive.R;
import com.example.jiaji.chauffeurprive.search.models.Feature;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;
import java.util.Locale;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class MapboxService {
    /**
     * The distance (in meters) within which to return place results.
     */
    private static final int PLACES_SEARCH_RADIUS = 10000;

    private String accessToken;

    private final Context context;
    private final MapboxApi mapboxApi;

    public MapboxService(Context context) {
        this.context = context;
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapboxApi = new Retrofit.Builder()
                .baseUrl("https://api.mapbox.com/geocoding/v5/")
                .client(new OkHttpClient())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(JacksonConverterFactory.create(objectMapper))
                .build().create(MapboxApi.class);
        accessToken = context.getString(R.string.access_token);
    }

    public Observable<List<Feature>> getAutocompletion(String input, String proximity) {
        return mapboxApi.getAutocompletion(input, "mapbox.places", accessToken, proximity, true, Locale.getDefault().getLanguage(), 6)
                .subscribeOn(Schedulers.io())
                .map(placeAutocomplete -> placeAutocomplete.features);
    }
}
