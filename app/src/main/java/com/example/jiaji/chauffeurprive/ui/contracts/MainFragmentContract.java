package com.example.jiaji.chauffeurprive.ui.contracts;

import com.example.jiaji.chauffeurprive.search.models.Feature;

import java.util.List;

public interface MainFragmentContract {

    interface View {
        void setAutocompletion(List<Feature> data);
    }

    interface UserActionListener {
        void getAutoCompletion(String queryString, double latitude, double longitude);
        void resetSearch();
    }

}
