package com.example.jiaji.chauffeurprive.search.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by Jiaji on 22/12/2017.
 */

public class Feature {
    public final String id;
    public final String text;
    public final String placeName;
    public final double[] center;
    public final Properties properties;

    @JsonCreator
    public Feature(@JsonProperty("id") String id, @JsonProperty("text") String text, @JsonProperty("place_name") String placeName, @JsonProperty("center") double[] center, @JsonProperty("properties") Properties properties) {
        this.id = id;
        this.text = text;
        this.placeName = placeName;
        this.center = center;
        this.properties = properties;
    }
}
