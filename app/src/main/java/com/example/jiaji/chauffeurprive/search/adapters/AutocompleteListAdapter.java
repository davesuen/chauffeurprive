package com.example.jiaji.chauffeurprive.search.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.jiaji.chauffeurprive.R;
import com.example.jiaji.chauffeurprive.search.models.Feature;

import java.util.ArrayList;
import java.util.List;

public class AutocompleteListAdapter extends RecyclerView.Adapter {

    private static final String TAG = "AutocompleteListAdapter";

    private List<Feature> predictionList;
    private LayoutInflater layoutInflater;

    public AutocompleteListAdapter(Context context) {
        layoutInflater = LayoutInflater.from(context);
        predictionList = new ArrayList<>();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public TextView subTitle;

        public ItemViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            subTitle = itemView.findViewById(R.id.subTitle);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = layoutInflater.inflate(R.layout.search_result, parent, false);
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
        itemViewHolder.title.setText(predictionList.get(position).text);
        if (predictionList.get(position).placeName.isEmpty()) {
            itemViewHolder.subTitle.setVisibility(View.GONE);
        } else {
            itemViewHolder.subTitle.setVisibility(View.VISIBLE);
            itemViewHolder.subTitle.setText(predictionList.get(position).placeName);
        }
    }

    public void setPredictionList(List<Feature> data) {
        this.predictionList.clear();
        this.predictionList.addAll(data);
        notifyDataSetChanged();
    }

    public void clearPredictionList() {
        this.predictionList.clear();
        notifyDataSetChanged();
    }

    /**
     * Get item by position
     * @param position don't know about header
     * @return item
     */
    public Feature getItem(int position) {
        return predictionList.get(position);
    }

    @Override
    public int getItemCount() {
        return predictionList.size();
    }

    public int getPredictionItemCount() {
        return predictionList.size();
    }
}
