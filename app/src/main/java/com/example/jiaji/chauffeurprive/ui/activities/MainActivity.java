package com.example.jiaji.chauffeurprive.ui.activities;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.example.jiaji.chauffeurprive.address.listeners.AddAddressListener;
import com.example.jiaji.chauffeurprive.address.listeners.AddressOnClickListener;
import com.example.jiaji.chauffeurprive.address.models.AddressItem;
import com.example.jiaji.chauffeurprive.map.models.LatLng;
import com.example.jiaji.chauffeurprive.ui.fragments.listeners.FragmentOnResumeListener;
import com.example.jiaji.chauffeurprive.R;
import com.example.jiaji.chauffeurprive.ui.fragments.AddressFragment;
import com.example.jiaji.chauffeurprive.ui.fragments.MainFragment;
import com.example.jiaji.chauffeurprive.utils.AddressHelper;
import com.mapbox.services.android.telemetry.permissions.PermissionsListener;
import com.mapbox.services.android.telemetry.permissions.PermissionsManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import java.util.List;

public class MainActivity extends AppCompatActivity implements PermissionsListener, NavigationView.OnNavigationItemSelectedListener, FragmentOnResumeListener, AddAddressListener, AddressOnClickListener {

    private static final String TAG = "MainActivity";

    private Toolbar toolbar;
    private ActionBarDrawerToggle toggle;
    private DrawerLayout drawer;
    private PermissionsManager permissionsManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);

        setupNavigationMenuButton();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        showMainFragment();

        AddressHelper.init(this);

        if (!PermissionsManager.areLocationPermissionsGranted(this)) {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionsResult");
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onPermissionResult(boolean granted) {
        Log.i(TAG, "onPermissionResult");
        if (granted) {
            Fragment mainFragment = getSupportFragmentManager().findFragmentByTag(MainFragment.TAG);
            ((MainFragment) mainFragment).activeLocation();
        } else {
            finish();
        }
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {

    }

    public void setupNavigationMenuButton() {
        Log.i(TAG, "setupNavigationMenuButton");
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, 0, 0);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (haveVisibleFragments()) {
                back();
            } else {
                finish();
            }
        }
    }

    private boolean haveVisibleFragments() {
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments == null || fragments.size() == 0) {
            return false;
        } else {
            for (Fragment fragment : fragments) {
                if (fragment.isVisible()) {
                    return true;
                }
            }
            return false;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void back() {
        Fragment addressFragment = getSupportFragmentManager().findFragmentByTag(AddressFragment.TAG);
        Fragment mainFragment = getSupportFragmentManager().findFragmentByTag(MainFragment.TAG);
        if (addressFragment != null && addressFragment.isVisible()) {
            hideFragment(addressFragment);
            setupNavigationMenuButton();
            if (mainFragment == null || !mainFragment.isVisible()) {
                showMainFragment();
            }
        } else {
            finish();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_address) {
            showAddressFragment();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }

    private void showMainFragment() {
        Log.i(TAG,"showMainFragment");
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment mainFragment = getSupportFragmentManager().findFragmentByTag(MainFragment.TAG);
        if (mainFragment != null) {
        /*
            if (((MainFragment) mainFragment).getFragmentOnResumeListener() == null) {
                ((MainFragment) mainFragment).setFragmentOnResumeListener(this);
            }
         */
            fragmentTransaction.show(mainFragment).commit();
        } else {
            mainFragment = MainFragment.newInstance();
            fragmentTransaction.addToBackStack(MainFragment.TAG);
            fragmentTransaction.add(R.id.fragmentContainer, MainFragment.newInstance(), MainFragment.TAG).show(mainFragment).commit();
        }
    }

    private void showAddressFragment() {
        Log.i(TAG, "showAddressFragment");
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment addressFragment = getSupportFragmentManager().findFragmentByTag(AddressFragment.TAG);
        if (addressFragment != null) {
            fragmentTransaction.show(addressFragment).commit();
        } else {
            addressFragment = AddressFragment.newInstance();
            fragmentTransaction.addToBackStack(AddressFragment.TAG);
            fragmentTransaction.add(R.id.fragmentContainer, AddressFragment.newInstance(), AddressFragment.TAG).show(addressFragment).commit();
        }
        showToolbarBackButton();
    }

    private boolean isShowingAddressFragment() {
        Fragment addressFragment = getSupportFragmentManager().findFragmentByTag(AddressFragment.TAG);
        return addressFragment != null && addressFragment.isVisible();
    }

    private void hideFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
        transaction.hide(fragment).commit();
    }

    public void showToolbarBackButton() {
        Log.i(TAG, "showToolbarBackButton");
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener((View view) -> back());
    }

    @Override
    public void onResumeFragment(Fragment fragment) {
        Log.i(TAG, "onResumeFragment");
        if (fragment.getTag().equals(MainFragment.TAG)) {
            if (fragment.isVisible()) {
                setupNavigationMenuButton();
            }
        } else {
            if (fragment.isVisible()) {
                showToolbarBackButton();
            }
        }
    }

    @Override
    public void onAddressAdded(AddressItem addressItem) {
        Fragment addressFragment = getSupportFragmentManager().findFragmentByTag(AddressFragment.TAG);
        if (addressFragment != null) {
            ((AddressFragment) addressFragment).addAddressItem(addressItem);
        }
        Toast.makeText(this, R.string.address_saved, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onAddressClicked(AddressItem addressItem) {
        hideFragment(getSupportFragmentManager().findFragmentByTag(AddressFragment.TAG));
        setupNavigationMenuButton();
        Fragment mainFragment = getSupportFragmentManager().findFragmentByTag(MainFragment.TAG);

        if (mainFragment == null || !mainFragment.isVisible()) {
            showMainFragment();
        }

        ((MainFragment) mainFragment).navigateToAddress(new LatLng(addressItem.getLatitude(), addressItem.getLongitude()));
    }
}