package com.example.jiaji.chauffeurprive.address.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Jiaji on 15/01/2018.
 */

public class AddressItem implements Parcelable {
    private double latitude;
    private double longitude;
    private String title;

    public AddressItem(double latitude, double longitude, String title) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.title = title;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeDouble(latitude);
        parcel.writeDouble(longitude);
        parcel.writeString(title);
    }

    public static final Parcelable.Creator<AddressItem> CREATOR
            = new Parcelable.Creator<AddressItem>() {
        public AddressItem createFromParcel(Parcel in) {
            return new AddressItem(in);
        }

        public AddressItem[] newArray(int size) {
            return new AddressItem[size];
        }
    };

    private AddressItem(Parcel in) {
        latitude = in.readDouble();
        longitude = in.readDouble();
        title = in.readString();
    }
}
